/*
	Manipulating Arrays with Array Method
		- JS has built in functions for arrays.
		- This allows us to manipulate and access array items.
		- Arrays can either be mutitaed or iterated
*/

/*
	Mutator Methods
		► are functions that "Mutate" or change an array after they're created.
		► These methods manipulate the original array ny performing various tasks such as adding and removing/deleting elements.

*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Lemon'];

/*
	push()
		► Adds an element in the end of an array AND returns the array's length

	Syntax:
		arrayName.push();
*/
console.log("Current array: ");
console.log(fruits);

// Adding Array Element using push()
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength); // 6
console.log("Mutated array from push method ");
console.log(fruits);

// Adding multiple elements to an array
fruits.push ('Avocado', 'Guava');
console.log(fruits);

/*
	pop()
		► Removes/Delete the last element in an array AND returns the removed element

	SYNTAX:
		arrayName.pop();

*/

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated array from pop() Method");
console.log(fruits);

/*
	unshift() ► is for Adding/inserting an element in the beginning
		► Add one or more elements at the beginning of an array

	SYntax:
		arrayName.unshift(ElementA); // Adding 1 Element
		arrayName.unshift(ElementA, ElementB); // Adding 1 or more Element
*/

// Adding ArrayElement in the Beginning of Array using unshift() method
//fruits.unshift('Lime', 'Banana');
let unshiftFruits = fruits.unshift('Lime', 'Banana');
console.log(unshiftFruits);
console.log('Mutated array from unshift method');
console.log(fruits);


/*
	shift() 
		► Removes an element at the beginning of an array AND returns the removed element

	SYNTAX:
		arrayName.shift();

*/

// Removing/Deleting the beginning or the 1st ArrayElement then returns/print the removed arrayElement 
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

/*
	splice();
		► Simultaneously removes an element from a specified index number and adds an element
	
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsTOBeADDed);
*/

fruits.splice(1,2, 'Lime', 'Chery');
console.log("Mutated array from splice method");
console.log(fruits);

// Deleting ArrayElement from index 1 then delete 2 arrayElement from index 1

fruits.splice(1, 2);
console.log(fruits);

// Inserting ArrayElement : Starting from index[2], 0 means there is none to delete, then the Grapes is the inserted ArrayElement form index[2]
fruits.splice(2,0, 'Grapes');
console.log(fruits);

// deleting or removing from index[3] - index[lastElement]
// fruits.splice(3); 
// console.log(fruits); 


/*

	sort();
		► Rearranges the array element in alphanumeric order

	SYNTAX:
		arrayName.sort();

*/
fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);


/*
let arrayWithNumber = [100, 20, "Jayson", "Carlos"];
arrayWithNumber.sort();
console.log(arrayWithNumber);
*/

/*
	reverse();
		► Reverse the order of array elements

	Syntax:
		arrayName.reverse();

*/

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

/*
	fruits.sort().reverse();
	console.log(fruits);
		
		► This is possible using chainMethod

*/


/*
	Non-Mutator Methods
		► Non-Mutator methods are functions that do not modify or change an array after they're created
		
		► These methods do not manipulate the original array performing various tasks

*/

let countries = ['US', 'PH', 'CA', 'TH', 'HK', 'PH', 'FR', 'DE']

/*

	indexOf();
		► Returns the index number of the first matching element found in an array.

		► if no match was found, the result will be -1.

		► The search process will be done from the first element proceeding to the last element

	SYNTAX:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex); // specific from starting Point of search

*/
// IndexOf()
let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf Method: " + firstIndex); // 1

let invalidCountry = countries.indexOf('HW');
console.log("Result of indexOf Method: " + invalidCountry); // -1


/*
	lastIndexOf();
		► Returns the index number of the last matching element found in an array
		► The search process will be done from last element proceeding to the first element

	SYntax:
		arrayName.LastIndexOf(searchValue);
		arrayName.LastIndexOf(searchValue, fromIndex); // specific from starting Point of search

*/

// lastIndexOf();
let lastIndex = countries.lastIndexOf('PH');
console.log("Result of LastIndexOf Method: " + lastIndex); // 5
console.log(countries);

/*
	slice();
		► Portions/Slices elements from an array AND returns a new Array

	SYntax:
		arrayName.slice(startingIndex); // if one
		arrayName.slice(startingIndex, endingIndex); // Multiple

*/
//Slicing off elements from a specified index to the last element

let slicedArrayA = countries.slice(2); // print the index[2] - index[lastElement] then the rest will not be printed
console.log('Result from Slice Method: ');
console.log(slicedArrayA);


// The endingIndex is not included to return/printed
// SLicing off elements from a specidied index to another index
// but the specified last index is not included in the return
let slicedArrayB = countries.slice(2, 4);
console.log('Result from Slice Method: ');
console.log(slicedArrayB);

/*
	toString();
		► Returns an array as a string by commas

	SYNTAX: 
		arrayName.toString();

*/
// toString();
let stringArray = countries.toString();
console.log('Result from toString() Method: ' + stringArray);

/*
	concat();
		► combines two array and returns the combined result

	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/
// concat();
let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale CSS', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];


//let tasks = taskArrayA.concat(taskArrayB).concat(taskArrayC);
let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

//Concat using two array
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

//Concat Usiing ELements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log(combinedTasks);


// Can we combine multiple arrays with elements? : YES
let multipleTasks = taskArrayA.concat(taskArrayB, taskArrayC, 'smell express', 'throw react' );
console.log(multipleTasks);

/*
	join();
		► Returns an array as a "String" seperated by  a specified seperator string

		Syntax:
			arrayName.join('separatorString');

*/
let users = ['Jack', 'Raf', 'Daphne', 'Lexus'];
console.log(users.join()); // (,) comma or default separator
console.log(users.join(' ')); // ( ) space seprator 
console.log(users.join(' - ')); // ( - ) dash sperator

/*
	Iteration Methods
		► Iteration methods are loos designed to perform repetitive on array.
		► Iteration methods loops over all items in an array.
*/

/*
	forEach()
		► Similar to a for loop that iterates on each array element.

		► For each item in the array, the anonymous function passed in the forEach(); Method will be run
		
		► Anonymous Function is a function without a name
	SYntax:
		arrayName.forEach(function(indiveElement){
			statement
		})
*/

console.log(multipleTasks);

					//anonymous function "parameter" represents each element of the array to be iterated
multipleTasks.forEach(function(task){ // (task)
	console.log(task);
})

// COnverting forEach() to for loop
for (let i = 0; i < multipleTasks.length; i++){
	console.log(multipleTasks[i]);
}
// Using forEach with conditional Statements
let fiteredTasks = [];

multipleTasks.forEach(function(task){

	if(task.length > 10) { // Adds all of mutipleTasks arrayElement with greater than (10) characters to another array
		
		fiteredTasks.push(task)
		
	}
})
console.log("Result of Filtered Task: ");
console.log(fiteredTasks);


/*
	map()
		- iterates on each element AND returns new array with different values depending on the result of the function's operation

		- unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation

	Syntax:
		let/const result = arrayName.map(function(indivElement) {})

*/

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){

	return number * number;

})
console.log("Original Array: ");
console.log(numbers);
console.log("Result of MAP Method: ");
console.log(numberMap);

// map() vs forEach
/*

	if you want to manipulate or update an existing array you can use the forEach but if you want to have a new arraylist from an existing array you can use map.

*/

let numberForEach = numbers.forEach(function(number){

	return number * number;

})
console.log(numberForEach);

/*
	every(); // it's like a AND (&&) operator
		- checks if all elements in an array meet the given condition Returns a true value if all elements meet the given condition and false if otherwise

	Syntax:
		let/const reult = arrayName.every(function(indivElement){
			return expression/condition
		})

*/

let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log("Result of every() method:");
console.log(allValid);

/*
	some(); // it's like a OR (||) operator
		- checks if at least one element in the array meets the given condition. Returns a true value if at least one elements meets the given condition and false if otherwise

	Syntax:
		let/const result = arrayName.some(function(indivElement){
			return expression/condition
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Result of some() method:");
console.log(someValid);



if(someValid){
	console.log("Some numbers in the array are less than 2");
}

if(allValid){
	console.log("All numbers in the array are less than 3");
}

/*
	filter();
		- return a new array that contains elements which meets the given condition. 

		- Return an empty array if no elements were found

	Syntax: 
		let/const resultArray = arrayName.filter(function(indivElement){
			return expression/condition;
		})

*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("The result of filter method:");
console.log(filterValid); // [1, 2]

// No elements found
let nothingFound = numbers.filter(function(number){
	return (number > 6)
});
console.log("The result of filter method:");
console.log(nothingFound);



//let numbers = [1,2,3,4,5];
// FIltering using forEach
let filteredNumbers = [];

numbers.forEach(function(number){
	if (number < 3) {
		filteredNumbers.push(number);
	}
	
});
console.log(filteredNumbers);

/*
	includes();
		► includes() Method checks if the argument passed can be found in the array.

			- it returns a boolean which can be saved in a variable
			- returns false if it is not
	SYNTAX:
		arrayName.include();

*/

let products = ['Mouse', 'Keyboard', 'Laptop',  'Monitor'];

let productFound = products.includes('Mouse');
console.log(productFound); // true

let productNotFound = products.includes('Headset');
console.log(productNotFound); // false

// Method Chaining
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
});
console.log(filteredProducts);